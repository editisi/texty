# README

## workflow

### Texty

*Logika:* Gitlab Milestone "simuluje" project, tedy pro kazdy text
bude project/milestone a *tracker* branch pro cely text, ke ktere
budou naparovany jednotlive branche/MR pro casti textu. Text se bude
mergovat do *main* jako celek ze *tracker* branch. Pokud je pred
nazvem MR 'Draft:', tak to oznacuje, ze se na neco ceka (patrne
review...); takto nam to dovoluje mit jakysi prehled o prubehu prace.

1. vytvor Milestone (Issue -> Milestones -> New milestone) s nazvem textu
2. vytvor *tracker* branch s nazvem textu, napr. `text-la-teoria`
2. pro kazdou cast textu pouzivej zvlast branch, napr. `text-la-teoria-ch01`
3. vytvor merge request (MR) pro cast textu v branchi, napr. `[TEXT] la teoria - ch01`
   vuci *tracker* branch, nikoli *main*!, a vyber spravny *milestone*
4. MR pro cast textu, ktery prosel review se zmerguje to *tracker* branche
5. ...
